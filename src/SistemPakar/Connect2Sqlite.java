/* Connect2Sqlite.java 
 * @author Dwi Kusmanto */

package SistemPakar;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Connect2Sqlite {
    private  Connection connection;
    Boolean connect;
    javax.swing.table.DefaultTableModel modelTableContent, modelTableIndex;

    
    public Connect2Sqlite(String file){
        try {
            Class.forName("org.sqlite.JDBC");
            try {
                connection = DriverManager.getConnection("jdbc:sqlite:" +file);
                Statement s = connection.createStatement();
                ResultSet r=null;
                
                r = s.executeQuery("SELECT * FROM table_index");
                    if(r.next()){
                        connect = true;
                    }else{
                        connect = false;
                        JOptionPane.showMessageDialog(null, "Kesalahan dalam file database !");
                    }  
                    s.close();
                    r.close();
            } catch (SQLException ex) {
                //GAGAL connection
                connect = false;
                JOptionPane.showMessageDialog(null, "Kesalahan dalam file database !");
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
        } catch (ClassNotFoundException ex) {
            //no driver
            connect = false;
            JOptionPane.showMessageDialog(null, "Kesalahan dalam file driver SQLITE, hubungi developer !");
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
//    public Connect2Sqlite(String filedir){
//        try {
//            Class.forName("org.sqlite.JDBC");
//            try {
//                connection = DriverManager.getConnection("jdbc:sqlite:" +filedir);
//                Statement s = connection.createStatement();
//                ResultSet r=null;
//                try{
//                    r = s.executeQuery("SELECT * FROM table_index");
//                    if(r.next()){
//                        connect = true;
//                    }else{
//                        connect = false;
//                    }  
//                }catch(SQLException ex){
//                    JOptionPane.showMessageDialog(null, "Kesalahan dalam file database !");
//                    connect = false;
//                    Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
//                }finally{
//                    s.close();
//                    r.close();
//                }
//            } catch (SQLException ex) {
//                JOptionPane.showMessageDialog(null, "Kesalahan dalam file database !");
//                connect = false;
//                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
//                
//            }
//        } catch (ClassNotFoundException ex) {
//            connect = false;
//            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
//            
//        }
//    }
    
    

 ///////ALL ABOUT GET?//////////   
    public boolean isConnect(){
        return connect;
    }
    public String getContent(String table_name, String code){
        try {  
            Statement s = connection.createStatement();
            ResultSet r = null;
            try {   
                r = s.executeQuery("SELECT * FROM " +table_name+ " WHERE code=" +code); 
                return r.getString("content");
            } catch (SQLException ex) {  
                //Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);   
                return null;
            }finally{                       
                r.close();  s.close();                
            }
        } catch (SQLException ex) {                
            //Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);   
            return null;
        } 
    }
        
    public String getDetail(String table_name, String code){  
        try {   
            Statement s = connection.createStatement();
            ResultSet r = null;
            try {   
                r = s.executeQuery("SELECT * FROM " +table_name+ " WHERE code=" +code); 
                return r.getString("detail");
            } catch (SQLException ex) {                    
                //Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);   
                return null;
            }finally{                       
                r.close();  s.close();                
            }       
        } catch (SQLException ex) {                
            //Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);   
            return null;
        } 
    }
        
    public String getTable(String table_title){
        try {   
            Statement s = connection.createStatement();
            ResultSet r = null;
            try {   
                r = s.executeQuery("SELECT * FROM  table_index WHERE table_title='" +table_title +"'"); 
                return r.getString("table_code");
            } catch (SQLException ex) {                    
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);  
                return null;
            }finally{                       
                r.close();  s.close();                
            }       
        } catch (SQLException ex) {                
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);   return null;
        } 
    }

    public String getTableTitle(String table_code){
        try {   
            Statement s = connection.createStatement();
            ResultSet r = null;
            try {   
                r = s.executeQuery("SELECT * FROM  table_index WHERE table_code='" +table_code +"'"); 
                return r.getString("table_title");
            } catch (SQLException ex) {                    
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);  
                return null;
            }finally{                       
                r.close();  s.close();                
            }       
        } catch (SQLException ex) {                
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);   return null;
        } 
    }

    public String getType(String table_name, String code){
        try {   
            Statement s = connection.createStatement();
            ResultSet r = null;
            try {   
                r = s.executeQuery("SELECT * FROM " +table_name+ " WHERE code=" +code); 
                return r.getString("type");
            } catch (SQLException ex) {                    
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);  
                return null;
            }finally{                       
                r.close();  s.close();                
            }       
        } catch (SQLException ex) {                
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);   return null;
        } 
    }
    
    public String getPass(String user){
        try {   
            Statement s = connection.createStatement();
            ResultSet r = null;
            try {   
                r = s.executeQuery("SELECT * FROM admin_data WHERE name=" +"'"+user+"'"); 
                return r.getString("password");
            } catch (SQLException ex) {                    
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);  
                return null;
            }finally{                       
                r.close();  s.close();                
            }       
        } catch (SQLException ex) {                
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);   return null;
        }       
    }
    public String gotoWhat(String table_name, String code){
        try {   
            Statement s = connection.createStatement();
            ResultSet r = null;
            try {   
                r = s.executeQuery("SELECT * FROM " +table_name+ " WHERE code=" +code); 
                return r.getString("type").substring(2,r.getString("type").length());
            } catch (SQLException ex) {                    
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);  
                return null;
            }finally{                       
                r.close();  s.close();                
            }       
        } catch (SQLException ex) {                
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);   return null;
        }  
    }
    
    public boolean notNull(String table_name, String code){
        try {   
            Statement s = connection.createStatement();
            ResultSet r = null;
            try {   
                r = s.executeQuery("SELECT * FROM " + table_name + " WHERE code='" + code + "'"); 
                if(r.next()){
                    return true;
                }else{
                    return false;
                }
            } catch (SQLException ex) {                    
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);  
                return false;
            }finally{                       
                r.close();  s.close();                
            }       
        } catch (SQLException ex) {                
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);   
            return false;
        } 
    }

    /////GENERATE COMBO BOX/////
    public void setComboOption(javax.swing.JComboBox cb){
        cb.removeAllItems();   
        try {  
            Statement s = connection.createStatement();
            ResultSet r = null;
            try {   
                r = s.executeQuery("SELECT * FROM table_index");                 
                cb.addItem("[Pilih Diagnosis]");                        
                while (r.next()) {                            
                    cb.addItem(r.getString("table_title"));                       
                }               
            } catch (SQLException ex) {                    
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);                
            }finally{                       
                r.close();  s.close();                
            }
        } catch (SQLException ex) {                
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);          
        } 
    }

    public void setComboOptionQuestion(javax.swing.JComboBox cb, String table_name){
        cb.removeAllItems();   
        try {  
            Statement s = connection.createStatement();
            ResultSet r = null;
            try {   
                r = s.executeQuery("SELECT * FROM " +table_name +" WHERE type='Q'");                 
                //cb.addItem("[Pilih Kode]");                        
                while (r.next()) {                            
                    cb.addItem(r.getString("code"));                       
                }               
            } catch (SQLException ex) {                    
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);                
            }finally{                       
                r.close();  s.close();                
            }
        } catch (SQLException ex) {                
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);          
        } 
    }

    public void setComboCode(javax.swing.JComboBox cb, String table_name){
        cb.removeAllItems();   
        try {  
            Statement s = connection.createStatement();
            ResultSet r = null;
            try {   
                r = s.executeQuery("SELECT * FROM " +table_name);                 
                cb.addItem("[Pilih Kode]");                        
                while (r.next()) {                            
                    cb.addItem(r.getString("code"));                       
                }               
            } catch (SQLException ex) {                    
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);                
            }finally{                       
                r.close();  s.close();                
            }
        } catch (SQLException ex) {                
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);          
        } 
    }
    


    /////////ALL ABOUT SAVE DATA//////////

    public String getSavedTable(String name){
        try {   
            Statement s = connection.createStatement();
            ResultSet r = null;
            try {   
                r = s.executeQuery("SELECT name,nowTable FROM save_data WHERE name=" +"'"+name+"'");
                return r.getString("nowTable");
            } catch (SQLException ex) {                    
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);  
                return null;
            }finally{                       
                r.close();  s.close();                
            }       
        } catch (SQLException ex) {                
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);   return null;
        }     
    }
    public String getSavedIndex(String name){
        try {   
            Statement s = connection.createStatement();
            ResultSet r = null;
            try {   
                r = s.executeQuery("SELECT name,nowIndex FROM save_data WHERE name=" +"'"+name+"'");
                return r.getString("nowIndex");
            } catch (SQLException ex) {                    
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);  
                return null;
            }finally{                       
                r.close();  s.close();                
            }       
        } catch (SQLException ex) {                
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);   return null;
        }  
    }
    
    public void saveData(String name,String nowTable, String nowIndex, String nowLog){
	String date;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar dt = Calendar.getInstance();
        date = df.format(dt.getTime());

        try {
            Statement s = connection.createStatement();
            int update = 0;
            try{
                update = s.executeUpdate("INSERT OR IGNORE INTO save_data "+"(name,date,nowTable,nowIndex,nowLog) VALUES ('"+name+"','"+date +"','"+nowTable +"','"+nowIndex +"','"+nowLog +"' )");
                if (update>0){
                    //System.out.println("Simpan diagnosis : " +name+"[OK]");
                    JOptionPane.showMessageDialog(null, "Diagnosis telah disimpan !");
                }
            }catch(SQLException ex){
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
                s.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void deleteSavedData(String name){
        try {
            Statement s = connection.createStatement();
            int update = 0;
            try{
                update = s.executeUpdate("DELETE FROM save_data WHERE name=" +"'" +name +"'");
                if (update>0){

                    JOptionPane.showMessageDialog(null, "Data berhasil dihapus !");
                }
            }catch(SQLException ex){
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
                s.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getSavedLog(String name){
        try {   
            Statement s = connection.createStatement();
            ResultSet r = null;
            try {   
                r = s.executeQuery("SELECT name,nowLog FROM save_data WHERE name=" +"'"+name+"'");
                return r.getString("nowLog");
            } catch (SQLException ex) {                    
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);  
                return null;
            }finally{                       
                r.close();  s.close();                
            }       
        } catch (SQLException ex) {                
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);   return null;
        } 
    }
    public void setTableSaveData(javax.swing.JTable table){
        table.removeAll();
        String data[][];
        String header[]={"Nama","Tanggal","Diagnosis"};
        int jmlBaris,baris;
        baris=0;
        try {
            Statement s;
            s = connection.createStatement();
            ResultSet r = null;
            try {
                r = s.executeQuery("SELECT COUNT(*) as jmlRow FROM save_data");
                jmlBaris = r.getInt("jmlRow");

                data= new String [jmlBaris][header.length];
                r.close();
                r = s.executeQuery("SELECT name, date, nowTable FROM save_data");
                baris=0;
                while(r.next()){
                    for(int j=0; j<header.length; j++){
                        data[baris][j] = r.getString(j+1);    
                    }
                    baris++;
                }
                for(int j=0;j<jmlBaris;j++){
                        data[j][2]=getTableTitle(data[j][2]);
                }
                modelTableContent = new javax.swing.table.DefaultTableModel(data,header);
                table.setModel(modelTableContent);
                table.repaint();
            } catch (SQLException ex) {
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
                modelTableContent = new javax.swing.table.DefaultTableModel(null,header);
                table.setModel(modelTableContent);
                table.repaint();
            }finally{
               r.close();
               s.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    ///////ALL ABOUT SET TABLE//////////
    public  void setTableContent(String table_name, javax.swing.JTable table){
        table.removeAll();
        String data[][];
        String header[] = {"Kode","Tipe","Isi","Detail",};
        int jmlBaris,baris;
        baris=0;
        try {
            Statement s;
            s = connection.createStatement();
            ResultSet r = null;
            try {
                r = s.executeQuery("SELECT COUNT(*) as jmlRow FROM " +table_name);
                jmlBaris = r.getInt("jmlRow");

                data= new String [jmlBaris][header.length];
                r.close();
                r = s.executeQuery("SELECT code, type, content, detail FROM " +table_name +" ORDER BY code ASC");
                baris=0;
                while(r.next()){
                    for(int j=0; j<header.length; j++){
                        data[baris][j] = r.getString(j+1);    
                    }
                    baris++;
                }
                modelTableContent = new javax.swing.table.DefaultTableModel(data,header);
                table.setModel(modelTableContent);
                table.repaint();
            } catch (SQLException ex) {
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
                modelTableContent = new javax.swing.table.DefaultTableModel(null,header);
                table.setModel(modelTableContent);
                table.repaint();
            }finally{
               r.close();
               s.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
        }

    public  void setTableIndex(javax.swing.JTable table){
        table.removeAll();
        String data[][];
        String header[] = {"Kode","Nama",};
        int jmlBaris,baris;
        baris=0;
        try {
            Statement s;
            s = connection.createStatement();
            ResultSet r = null;
            try {
                r = s.executeQuery("SELECT COUNT(*) as jmlRow FROM table_index ");
                jmlBaris = r.getInt("jmlRow");

                data= new String [jmlBaris][header.length];
                r.close();
                r = s.executeQuery("SELECT * FROM table_index ");
                baris=0;
                while(r.next()){
                    for(int j=0; j<header.length; j++){
                        data[baris][j] = r.getString(j+1);    
                    }
                    baris++;
                }
                modelTableContent = new javax.swing.table.DefaultTableModel(data,header);
                table.setModel(modelTableContent);
                table.repaint();
            } catch (SQLException ex) {
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
                modelTableContent = new javax.swing.table.DefaultTableModel(null,header);
                table.setModel(modelTableContent);
                table.repaint();
            }finally{
               r.close();
               s.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
    public void setTableUserList(javax.swing.JTable table){
        table.removeAll();
        String data[][];
        String header[]={"Username"};
        int jmlBaris,baris;
        baris=0;
        try {
            Statement s;
            s = connection.createStatement();
            ResultSet r = null;
            try {
                r = s.executeQuery("SELECT COUNT(*) as jmlRow FROM admin_data");
                jmlBaris = r.getInt("jmlRow");

                data= new String [jmlBaris][header.length];
                r.close();
                r = s.executeQuery("SELECT name FROM admin_data");
                baris=0;
                while(r.next()){
                        data[baris][0] = r.getString(1);    
                    baris++;
                }
                modelTableContent = new javax.swing.table.DefaultTableModel(data,header);
                table.setModel(modelTableContent);
                table.repaint();
            } catch (SQLException ex) {
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
                modelTableContent = new javax.swing.table.DefaultTableModel(null,header);
                table.setModel(modelTableContent);
                table.repaint();
            }finally{
               r.close();
               s.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    ///////EDIT & DELETE DATA//////////////////
    public void edit_content(String table_name, String code, String new_type, String new_content, String new_detail){
        try {
            Statement s = connection.createStatement();
            int update = 0;
            try{
                update = s.executeUpdate("UPDATE " +table_name +" SET type='" +new_type +"', content='"+new_content +"', detail='" +new_detail +"' WHERE code=" +code);
                if (update>0){
                    JOptionPane.showMessageDialog(null, "Data berhasil diedit !");
                }
            }catch(SQLException ex){
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
                s.close();
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void delete(String table_name, String code){
        try {
            Statement s = connection.createStatement();
            int update = 0;
            try{
                update = s.executeUpdate("DELETE FROM " +table_name +" WHERE code='" +code +"'");
                if (update>0){

                }
            }catch(SQLException ex){
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
                s.close();
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void single_delete(String table_name, String code){
        try {
            Statement s = connection.createStatement();
            int update = 0;
            try{
                update = s.executeUpdate("DELETE FROM " +table_name +" WHERE code='" +code +"'");
                if (update>0){

                    JOptionPane.showMessageDialog(null, "Data berhasil dihapus !");
                }
            }catch(SQLException ex){
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
                s.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    

    public void addNewContent(String table_name, String code, String type, String content, String detail){
        try {
            Statement s = connection.createStatement();
            int update = 0;
            try{
                update = s.executeUpdate("INSERT OR IGNORE INTO " +table_name +"(code,type,content,detail) VALUES ('"+code+"','"+type +"','"+content +"','"+detail+"' )");
                if (update>0){

                    JOptionPane.showMessageDialog(null, "Data baru teleh ditambahkan !");
                }
            }catch(SQLException ex){
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
                s.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void newTable(String table_name){
        try {
            Statement s = connection.createStatement();
            int update = 0;
            try{
                update = s.executeUpdate("CREATE TABLE '" + table_name + "' ('code' VARCHAR NOT NULL  UNIQUE , 'type' VARCHAR NOT NULL , 'content' TEXT NOT NULL , 'detail' TEXT NOT NULL  DEFAULT 'No detail.')");
                if (update>0){

                }
            }catch(SQLException ex){
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
                s.close();
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addIndex(String table_name, String title){
        try {
            Statement s = connection.createStatement();
            int update = 0;
            try{
                update = s.executeUpdate("INSERT INTO table_index (table_code,table_title) VALUES('"+table_name +"','"+title +"')");
                if (update>0){

                }
            }catch(SQLException ex){
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
                s.close();
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void addUser(String user, String password){
        try {
            Statement s = connection.createStatement();
            int update = 0;
            try{
                update = s.executeUpdate("INSERT INTO admin_data (name,password) VALUES('"+user +"','"+password +"')");
                if (update>0){

                    JOptionPane.showMessageDialog(null, "User baru telah ditambahkan");
                }
            }catch(SQLException ex){
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
                s.close();
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void editUser(String oldUsername, String newUsername, String newPass){
        try {
            Statement s = connection.createStatement();
            int update = 0;
            try{
                update = s.executeUpdate("UPDATE admin_data  SET name='" +newUsername +"', password='"+newPass +"' WHERE name='" +oldUsername+"'");
                if (update>0){
                    JOptionPane.showMessageDialog(null, "Data berhasil diedit !");
                }
            }catch(SQLException ex){
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
                s.close();
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void deleteFromIndex(String table_name){
        try {
            Statement s = connection.createStatement();
            int update = 0;
            try{
                update = s.executeUpdate("DELETE FROM table_index WHERE table_code='" + table_name + "'");
                if (update>0){

                }
            }catch(SQLException ex){
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
                s.close();
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void deleteUser(String user){
        try {
            Statement s = connection.createStatement();
            int update = 0;
            ResultSet r = null;
            try{
                r = s.executeQuery("SELECT COUNT (*) as jmlUser FROM admin_data");
                int jmlUser=r.getInt("jmlUser");
                r.close();
                s.close();
                
                if(jmlUser>1){
                    update = s.executeUpdate("DELETE FROM admin_data WHERE name='" + user + "'");
                    if (update>0){
                    //System.out.println("DELETE : " +user+" [OK]");  
                    }
                }else{
                    //System.out.println("DELETE : " +user+" [CANCELLED]");
                }
                
            }catch(SQLException ex){
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
                s.close();
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public int getJmlUser(){
        int jmlUser=0;
        try {
            Statement s = connection.createStatement();
            ResultSet r = null;
            try{
                r = s.executeQuery("SELECT COUNT (*) as jmlUser FROM admin_data");
                jmlUser=r.getInt("jmlUser");
                return jmlUser;
            }catch(SQLException ex){
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
                return jmlUser;
            }finally{
                r.close();
                s.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
            return jmlUser;
        }
    }
    public void deleteTable(String table_name){
        try {
            Statement s = connection.createStatement();
            int update = 0;
            try{
                update = s.executeUpdate("DROP TABLE " + table_name);
                if (update>0){
                    //System.out.println("DELETE : " +table_name+"[OK]");
                }
            }catch(SQLException ex){
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
                s.close();
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void delete_all_after(String table_name, String code){
        try{
            Statement s = connection.createStatement();
            ResultSet r = null;
            try{
                r = s.executeQuery("SELECT COUNT(*) as jmlRow FROM " +table_name+ " WHERE code like '" +code +"%' ORDER BY code");
                int jmlBaris = r.getInt("jmlRow")-1;
                
                r.close();
                
                r = s.executeQuery("SELECT * FROM " +table_name+ " WHERE code like '" +code +"%' ORDER BY code");
                String codes[] = new String[jmlBaris];
                
                r.next();
                int i =0;

                while(r.next()){
                    codes[i] = r.getString("code");

                    i=i+1;
                }
                r.close();
                
                for(int x =0;x<codes.length;x++){
                    int update =0;
                    update = s.executeUpdate("DELETE FROM " +table_name +" WHERE code='"+codes[x] +"'");
                    if(update>0){
                    }
                }

            }catch(SQLException ex){
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
                r.close();
                s.close();
            }
        }catch(SQLException ex){
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void edit_to_place_after_yes(String table_name, String code){
        int panjang_kode = code.length();
        try{
            Statement s = connection.createStatement();
            ResultSet r = null;
            try{
                r = s.executeQuery("SELECT COUNT(*) as jmlRow FROM " +table_name+ " WHERE code like '" +code +"%' ORDER BY code");
                int jmlBaris = r.getInt("jmlRow");

                r.close();
                
                r = s.executeQuery("SELECT * FROM " +table_name+ " WHERE code like '" +code +"%' ORDER BY code DESC");
                String codes[] = new String[jmlBaris];
                int i =0;
                while(r.next()){
                    codes[i]=r.getString("code");
                    i=i+1;
                }
                r.close();
                
                String kode;
                for( int x=0; x<codes.length;x++){
                    kode = code +"1" +codes[x].substring(panjang_kode);
                    //System.out.println(codes[x] +" DIEDIT JADI = " +kode);
                    int update = s.executeUpdate("UPDATE " +table_name +" SET code='" +kode +"' WHERE code=" +codes[x]);
                    if(update>0){
                        //System.out.println(codes[x] +" DIEDIT JADI = " +kode +"[OK]");
                    }
                }
            }catch(Exception ex){
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
                r.close();s.close();
            }
        }catch(SQLException ex){
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void edit_to_place_after_no(String table_name, String code){
        int panjang_kode = code.length();
        
        try{
            Statement s = connection.createStatement();
            ResultSet r = null;
            try{
                r = s.executeQuery("SELECT COUNT(*) as jmlRow FROM " +table_name+ " WHERE code like '" +code +"%' ORDER BY code");
                int jmlBaris = r.getInt("jmlRow");

                r.close();
                
                r = s.executeQuery("SELECT * FROM " +table_name+ " WHERE code like '" +code +"%' ORDER BY code DESC");
                String codes[] = new String[jmlBaris];
                int i =0;
                while(r.next()){
                    codes[i]=r.getString("code");
                    i=i+1;
                }
                r.close();
                
                String kode;
                for( int x=0; x<codes.length;x++){
                    kode = code +"0" +codes[x].substring(panjang_kode);

                    int update = s.executeUpdate("UPDATE " +table_name +" SET code='" +kode +"' WHERE code=" +codes[x]);
                    if(update>0){

                    }
                }
            }catch(Exception ex){
                Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
            }finally{
                r.close();s.close();
            }
        }catch(SQLException ex){
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void close(){
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Connect2Sqlite.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}