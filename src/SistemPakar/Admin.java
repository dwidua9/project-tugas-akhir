/*
 * Admin.java
 * @author Dwi Kusmanto
 */

package SistemPakar;

import javax.swing.JOptionPane;


public class Admin extends Connect2Sqlite {
    private String nowTable, nowIndex, nowType, nowContent, nowDetail, nowTableTitle, nowLog="";

    public Admin(String filedir){
        super(filedir);
        if(super.isConnect()){
            nowLog = nowLog +"Open Database file : " +filedir +"[OK] \n";
        }else{
            nowLog = nowLog +"Open Database file : " +filedir +"[FAILED] \n";
        }
    }

    public void start(String table_title){
        nowTable = super.getTable(table_title);
        nowTableTitle = super.getTableTitle(nowTable);
        if (super.notNull(nowTable, "1")){
            nowIndex = "1";
            nowType = super.getType(nowTable, nowIndex);
            nowContent = super.getContent(nowTable, nowIndex);
            nowDetail = super.getDetail(nowTable, nowIndex);
            nowLog=nowLog + "Start ->" +nowTableTitle +"\n";
            nowLog = nowLog + nowContent;
        }else{
            nowIndex="1";
            nowType=null;
            nowContent=null;
            nowDetail=null;

        }
    }
    public void restart(){
        nowLog = nowLog+"\n";
        nowType = super.getType(nowTable, nowIndex);
        nowContent = super.getContent(nowTable, nowIndex);
        nowDetail = super.getDetail(nowTable, nowIndex);
        nowLog = nowLog + nowContent;
    }

    public void startAdvance(String table_title){
        nowTable = super.getTable(table_title);
        nowTableTitle = super.getTableTitle(nowTable);
    }
    public String getLog(){
        return nowLog;
    }
    public void yes(){
        nowLog = nowLog +"[YES]\n";
        if(nowType.startsWith("G")){
            nowTable = super.gotoWhat(nowTable, nowIndex);
            nowTableTitle = super.getTableTitle(nowTable);
            start(nowTableTitle);

        }else{
            nowIndex= nowIndex +"1";
            nowType = super.getType(nowTable, nowIndex);
            nowContent = super.getContent(nowTable, nowIndex);
            nowDetail = super.getDetail(nowTable, nowIndex);
        }
        nowLog = nowLog + nowContent;

    }

    public void no(){
        nowLog = nowLog+"[NO]\n";
        nowIndex= nowIndex +"0";
        nowType = super.getType(nowTable, nowIndex);
        nowContent = super.getContent(nowTable, nowIndex);
        nowDetail = super.getDetail(nowTable, nowIndex);
        nowLog = nowLog + nowContent;
    }

    public void back(){
        nowLog = nowLog.concat("\n[BACK]\n");
        nowIndex = nowIndex.substring(0, nowIndex.length()-1);
        nowType = super.getType(nowTable, nowIndex);
        nowContent = super.getContent(nowTable, nowIndex);
        nowDetail = super.getDetail(nowTable, nowIndex);
    }

    public void refresh(){
            nowType = super.getType(nowTable, nowIndex);
            nowContent = super.getContent(nowTable, nowIndex);
            nowDetail = super.getDetail(nowTable, nowIndex);
    }
    
    

    public void basicEdit(String table_name, String code, String type, String new_type, String new_content, String new_detail, boolean deleteallafter){
          //NEW CONTENT : QUESTION
        
        if (new_type.equals("Q")){
            //CURRENT CONTENT : QUESTION
            //QUESTION -> QUESTITON
            if(type.equals("Q")){
                super.edit_content(table_name, code, new_type, new_content, new_detail);
            }
            //CURRENT CONTENT = ANSWER
            //ANSWER -> QUESTION
            else if(type.equals("A")){
                super.edit_content(table_name, code, new_type, new_content, new_detail);
            }
            //CURRENT CONTENT = GOTO
            //GOTO -> QUESTION
            else if(type.substring(1, 1).equals("G")){
                super.edit_content(table_name, code, new_type, new_content, new_detail);
            }

        }
        //NEW DATA --> ANSWER
        else if(new_type.equals("A")){
            //CURRENT CONTENT = QUESTION
            //QUESTION -> ANSWER
            if(type.equals("Q")){
                super.edit_content(table_name, code, new_type, new_content, new_detail);
                if(deleteallafter){
                    super.delete_all_after(table_name, code);
                }
            }
            //CURRENT CONTENT = ANSWER
            // ANSWER -> ANSWER
            else if(type.equals("A")){
                super.edit_content(table_name, code, new_type, new_content, new_detail);
            }
            //CURRENT CONTENT = GOTO
            //GOTO -> ANSWER
            else if(type.substring(1, 1).equals("G")){
                super.edit_content(table_name, code, new_type, new_content, new_detail);
            }
        }
        //NEW DATA --> GOTO
        else if (new_type.equals("G.")){
            //CURRENT CONTENT = QUESTION
            //QUESTION -> GOTO
            if(type.equals("Q")){
                super.edit_content(table_name, code, new_type, new_content, new_detail);
                if(deleteallafter){
                    super.delete_all_after(table_name, code);
                }
            }
            //CURRENT CONTENT = ANSWER
            // ANSWER -> GOTO
            else if(type.equals("A")){
                super.edit_content(table_name, code, new_type, new_content, new_detail);
            }
            //CURRENT CONTENT = GOTO
            //GOTO -> GOTO
            else if(type.substring(1, 1).equals("G")){
                super.edit_content(table_name, code, new_type, new_content, new_detail);
            }
        }
    }

    public void advanceEdit(String table_name, String code, String new_type, String new_content, String new_detail){
        super.edit_content(table_name, code, new_type, new_content, new_detail);
    }

    public void advanceDelete(String table_name, String code){
        single_delete(table_name, code);
    }

    public void advanceAdd(String table_name, String code, String type, String content, String detail){
        super.addNewContent(table_name,code, type, content, detail);

    }



    public String getNowIndex(){
        return nowIndex;
    }

    public String getNowTable(){
        return nowTable;
    }

    public String getNowTableTitle(){
        return nowTableTitle;
    }

    public String getNowContent(){
        return nowContent;
    }

    public String getNowType(){
        return nowType;
    }

    public String getNowDetail(){
        return nowDetail;
    }
    
    public void basicAdd(String table_name, String code, String type, String content, String detail, Boolean noPrev, String moreOption){
        if (noPrev){
            //THERE ARE DATA WITH code = 1
            if (notNull(getNowTable(), "1")){
                if(moreOption.equals("delete")){
                    //DELETE DATA AFTER THE CODE=1
                    delete_all_after(getNowTable(), "1");
                    //DELETE THE CODE=1
                    delete(getNowTable(), "1");


                    //CREATE NEW DATA
                    addNewContent(getNowTable(),code, type, content, detail);

                }else if(moreOption.equals("placeAfterYes")){
                    //EDIT CODE
                    edit_to_place_after_yes(getNowTable(), "1");

                    //CREATE NEW DATA
                    addNewContent(getNowTable(),code, type, content, detail);
                }else if(moreOption.equals("placeAfterNo")){
                    //EDIT CODE
                    edit_to_place_after_no(getNowTable(), "1");

                    //CREATE NEW DATA
                    addNewContent(getNowTable(),code, type, content, detail);
                }
            }
            // NO DATA WITH CODE=1,
            else{
                addNewContent(getNowTable(),code, type, content, detail);
            }
        }else{
            //THERE ARE DATA WITH code = KODE
            if (notNull(getNowTable(), code)){
                if(moreOption.equals("delete")){
                    //DELETE DATA AFTER THE CODE=code
                    delete_all_after(getNowTable(), code);
                    //DELETE THE CODE=1
                    delete(getNowTable(), code);


                    //CREATE NEW DATA
                    addNewContent(getNowTable(),code, type, content, detail);

                }else if(moreOption.equals("placeAfterYes")){
                    //EDIT CODE
                    edit_to_place_after_yes(getNowTable(), code);

                    //CREATE NEW DATA
                    addNewContent(getNowTable(),code, type, content, detail);
                }else if(moreOption.equals("placeAfterNo")){
                    //EDIT CODE
                    edit_to_place_after_no(getNowTable(), code);

                    //CREATE NEW DATA
                    addNewContent(getNowTable(),code, type, content, detail);
                }
            }
            // NO DATA WITH CODE=1,
            else{
                addNewContent(getNowTable(),code, type, content, detail);
            }
        }
    }

    public void newDetection(String table_name, String title){
        addIndex(table_name, title);
        newTable(table_name);
        JOptionPane.showMessageDialog(null, "Diagnosis baru telah dibuat !");
    }
    public void deleteDetection(String table_name){
        deleteFromIndex(table_name);
        deleteTable(table_name);
        JOptionPane.showMessageDialog(null, "Satu jenis diagnosis telah dihapus !");
        
    }
}
