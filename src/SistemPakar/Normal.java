/*
 * Normal.java
 * @author Dwi Kusmanto
 */

package SistemPakar;

public class Normal extends Connect2Sqlite  {
    private String nowTable, nowIndex, nowType, nowContent, nowDetail, nowTableTitle, nowLog="";


    public Normal(String filedir){
        
        super(filedir);
        if(super.isConnect()){
            nowLog = nowLog +"Koneksi database : " +filedir +"[SUKSES]"+"\n";
        }else{
             nowLog = nowLog +"Koneksi database : " +filedir +"[GAGAL]"+"\n";
        }
    }

    public void open(String table, String index, String log){
        nowTable = table;
        nowTableTitle=super.getTableTitle(table);
        nowIndex = index;
        nowLog = log;
        nowType = super.getType(nowTable, nowIndex);
        nowContent = super.getContent(nowTable, nowIndex);
        nowDetail = super.getDetail(nowTable, nowIndex);

    }


    public String getLog(){
        return nowLog;
    }

    public void start(String table_title){
        nowTable = super.getTable(table_title);
        nowTableTitle = super.getTableTitle(nowTable);
        nowIndex = "1";
        nowType = super.getType(nowTable, nowIndex);
        nowContent = super.getContent(nowTable, nowIndex);
        nowDetail = super.getDetail(nowTable, nowIndex);
        //nowLog=nowLog + "Start ->" +nowTableTitle +"\n";

        nowLog = nowLog + "[Mulai diagnosis]" +nowTableTitle +"\n";
        
        nowLog = nowLog + nowContent;
    }


    public void yes(){

        nowLog = nowLog +"[YA]" +"\n";
        
        //nowLog = nowLog +"[YES]\n";
        if(nowType.startsWith("G")){
            nowTable = super.gotoWhat(nowTable, nowIndex);
            nowTableTitle = super.getTableTitle(nowTable);
            start(nowTableTitle);

        }else{
            nowIndex= nowIndex +"1";
            nowType = super.getType(nowTable, nowIndex);
            nowContent = super.getContent(nowTable, nowIndex);
            nowDetail = super.getDetail(nowTable, nowIndex);
        }
        nowLog = nowLog + nowContent;

    }

    public void no(){
        nowLog = nowLog +"[NO]" +"\n";
        
        nowIndex= nowIndex +"0";
        nowType = super.getType(nowTable, nowIndex);
        nowContent = super.getContent(nowTable, nowIndex);
        nowDetail = super.getDetail(nowTable, nowIndex);
        nowLog = nowLog + nowContent;
    }

    public void back(){
        nowLog = nowLog +"\n[KEMBALI]\n";
        
        
        nowIndex = nowIndex.substring(0, nowIndex.length()-1);
        nowType = super.getType(nowTable, nowIndex);
        nowContent = super.getContent(nowTable, nowIndex);
        nowDetail = super.getDetail(nowTable, nowIndex);
    }

    public String getNowIndex(){
        return nowIndex;
    }

    public String getNowTable(){
        return nowTable;
    }

    public String getNowTableTitle(){
        return nowTableTitle;
    }

    public String getNowContent(){
        return nowContent;
    }

    public String getNowType(){
        return nowType;
    }

    public String getNowDetail(){
        return nowDetail;
    }

}
