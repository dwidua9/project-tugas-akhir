/*
 * Main.java
 * @author Dwi Kusmanto
 */
package SistemPakar;

import SistemPakar.gui.UserMode;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Dwi Kusmanto
 */
public class Main {
    public static UserMode normalMode;
    public static void main(String[] args) {
    try {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch (UnsupportedLookAndFeelException e) {

    }
    catch (ClassNotFoundException e) {

    }
    catch (InstantiationException e) {

    }
    catch (IllegalAccessException e) {

    }
    
    normalMode = new UserMode();

    Toolkit toolkit = normalMode.getToolkit();
    Dimension screenSize=toolkit.getScreenSize();
    normalMode.setLocation((screenSize.width/2)-(normalMode.getWidth()/2), 7);
    normalMode.setVisible(true);
    
    }
}
